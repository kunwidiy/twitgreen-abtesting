<?php
	include 'module/headerv4.php';
	//include 'module/infographic-global.php';
	
?>
	
	<div class="prlx-3"></div>

	<div class="container">

		<div class="twit-page-header">
			<h1 class="text-white">Dukung Project</h1>
			<p class="lead text-white">adalah aksi memberi dukungan kepada Relawan Gerakan Menanam Pohon dan Petani yang terlibat dengan cara memberikan nama kepada tiga pohon yang telah ditanam.</p>
			<p>&nbsp;</p>
			<p><button class="btn btn-large btn-success" type="button"><i class="fa fa-male"></i> &nbsp; Bergabung</button></p>
			<p>&nbsp;</p>
		</div>

		<div>
			
			<div class="center">
				<h1>Menjadi Sponsor</h1>
				<p><img src="img/gabung-step.png"></p>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
			</div>
			
			<div class="row">
				<div class="span9">
					<h2>Bagaimana cara menjadi relawan?</h2>
					
				</div>


				<div class="span3">
					
				</div>
			</div>
			
			

		</div>

	</div>

	<!-- call to action start project-->
	<div class="twit-how-prj-box">
		<div class="container">
			<div class="center">
				<h2>Tertarik?</h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod <br> tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
				<br/>
				<p><button class="btn btn-large btn-success" type="button"><i class="fa fa-male"></i> &nbsp;<i class="fa fa-plus"></i> Bergabung</button></p>
			</div>
		</div>
	</div>
	<!-- /call to action start project-->
	
	
	

<?php
	include 'module/footerv4.php';
?>