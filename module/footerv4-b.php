<?php
/**
 * The template for displaying the footer.
 */
?>

 <!-- footer -->
	<div class="footer-widget-wrapper">
		<div class="container">

			
				<div class="row">

					<div class="span6">
						<h4 class="text-white">Twitgreen</h4>
						<p class="text-white">
							Untuk mewujudkan visi ramah lingkungan berkelanjutan, Gerakan Sobat Bumi yang dirintis oleh Pertamina Foundation (yayasan yang didanai PT Pertamina (persero)) menerapkan strategi gerakan berbasis teori narasi “Sobat Bumi” yang diyakini sebagai cerita bersama setiap orang pendamba visi tersebut.
						</p>
						<p class="text-white">
							Gerakan ini berakar pada kearifan lokal Bangsa Indonesia untuk Dunia yang berkelanjutan merupakan gerakan global untuk memerangi musuh dunia “Global Warming” dengan melakukan aksi lokal “greenomic” yang tertujuan memperbaiki lingkungan dan kesejahteraan lokal secara berkelanjutan. 
						</p>
						<p class="text-white">
							Ayo dukung <a href="#">www.twitgreen.com</a> agar menjadi sosial media dan aplikasi “crowd funding” menabung pohon yang menarik Pendukung sehingga menarik para donatur untuk membantu kegiatan ini.
						</p>	
					</div>

					<div class="span3">
						<h4 class="text-white">About Us</h4>
						<ul class="footer-widget-list">
							<li><a href="#">Apa itu TwitGreen?</a></li>
							<li><a href="#">Tutorial Penggunaan</a></li>
							<li><a href="#">Tutorial Akun Baru</a></li>
							<li><a href="#">Blog</a></li>
						</ul>
						<div class="alert alert-success">
						  <h5>Some TExt</h5>
						  Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
						</div>
					</div>

					<div class="span3">
						<h4 class="text-white">Tweet TwitGreen</h4>
						<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/sobatbumi" data-widget-id="306920087988862976">Tweets by @sobatbumi</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


					</div>
			</div>



		</div>
	</div>

			<div class="footer-wrapper">
				<div class="container">
					<div class="row">
						<div id="" class="span6">
							<p class="text-white">Supported by Pertamina Foundation & Sobat Bumi</p>
						</div>
						<div id="" class="span6">
							<p class="text-white pull-right">&copy; Twitgreen.com 2013</p>
						</div>
					</div>
				</div>
			</div>
 <!-- /footer -->
	
	 <!-- Le javascript
	================================================== -->
   <!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="js/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="js/jquery.bootstrap.wizard.js"></script>
	<!-- add new js twitgreen v4 -->
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>



	 
<script>

	//home project box slider

	$('.bxslider').bxSlider({
	  minSlides: 1,
	  maxSlides: 4,
	  slideWidth: 220,
	  slideMargin: 20
	});

	//Paparalakan
	$(document).ready(function() {
		$(window).scroll(function(){
			$('*[class^="prlx"]').each(function(r){
				var pos = $(this).offset().top;
				var scrolled = $(window).scrollTop();
		    	$('*[class^="prlx"]').css('top', -(scrolled * 0.5) + 'px');			
		    });
		});
	});
		
</script>


	
</body></html>