<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Twitgreen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 50px;
        padding-bottom: 40px;
      }
    </style>

    	<link href="css/bootstrap-responsive.css" rel="stylesheet">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/datepicker.css">
		<link href="css/style.css" rel="stylesheet">

		<!-- this new redesign style v4-->
		<link href="css/jquery.bxslider.css" rel="stylesheet">
		<link href="css/font-awesome.css" rel="stylesheet">
		<link href="css/WOAHbar.css" rel="stylesheet">
		<link href="css/new-home-style.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>
  
  <div id="fb-root"></div>

  <div class="woahbar" style="display: none;"><span><i class="fa fa-comment"></i> Contact Person: &nbsp;<i class="fa fa-mobile"></i>&nbsp;&nbsp;  +62 812 24507 235 &nbsp;&nbsp; <button class="btn btn-primary" type="button"><i class="fa fa-paper-plane"></i> Hubungi Kami</button> </span> <a class="close-notify" onclick="woahbar_hide();"><img class="woahbar-up-arrow" src="images/woahbar-up-arrow.png" /></a></div>
  <div class="woahbar-stub" style="display: none;"><a class="show-notify" onclick="woahbar_show();"><img class="woahbar-down-arrow" src="images/woahbar-down-arrow.png" /></a></div>

    <div class="navbar navbar-fixed-top">
	  <div class="navbar-inner twt4-bar">
	    <div class="container">
	 
	      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
	      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </a>
	 
	      <!-- Be sure to leave the brand out there if you want it shown -->
	      <a class="brand twit-brand" href="#">TwitGreen</a>
	 
	      <!-- Everything you want hidden at 940px or less, place within here -->
	      <div class="nav-collapse collapse">
	        <!-- .nav, .navbar-search, .navbar-form, etc -->
	        <ul class="nav">
		      <li><a href="#" class="topnav2"><i class="fa fa-male"></i> Relawan</a></li>
		      <li><a href="#" class="topnav2"><i class="fa fa-tree"></i> Buat Project</a></li>
		    </ul>
		    <form class="navbar-search pull-left">
			  <input type="text" class="search-query" placeholder="Cari Project">
			</form>

			
			  
			  	<div class="btn-group pull-right">
				  <button class="btn btn-success topnav"> <i class="fa fa-key"></i> Login</button>
				  <button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="#"><i class="fa fa-globe"></i> Login Sobatbumi</a></li>
		      		<li><a href="#"><i class="fa fa-facebook-square"></i> Login Facebook</a></li>
		      		<li class="divider"></li>
		      		<li><a href="#"><i class="fa fa-heart"></i> Register</a></li>
				  </ul>
				</div>
			  
			

	      </div>
	 
	    </div>
	  </div>
	</div>
	
			
	