<?php
	include 'module/headerv4.php';
	//include 'module/infographic-global.php';
	
?>
	<div class="prlx-1"></div>
	<div class="big-map-wrapper">

		<!-- Big map twitgreen just2-->
		<div class="big-maps-wrapper">
			<div class="Flexible-container">

				<iframe width='100%' height='500px' frameBorder='0' src='http://a.tiles.mapbox.com/v3/sigitsedayu.i9eao9pa/attribution,zoompan,zoomwheel,geocoder,share.html'></iframe>
				
			</div>
		</div>
		<!-- /Big map twitgreen-->

		<!-- featured projects twitgreen-->
		<div class="big-project-wrapper">
			<div class="container">

				<!-- list project slider twitgreen-->
				<div class="new-featured-prjc">
					<ul class="bxslider">
					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopertamina.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>
					  
					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopatra.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logojkt.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopertamina.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopatra.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project center">
								<img src="upload/logojkt.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>
					</ul>
				</div> <!-- /new-featured-prjc-->
				<!-- /list project slider twitgreen-->
			
			</div>
		</div>
		<!-- /featured projects twitgreen-->

	</div>

	<!-- call to action start project-->
	<div class="twit-how-prj-box">
		<div class="container">
			<div class="center">
				<h2>Bagaimana memulai Project?</h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod <br> tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
				<br/>
				<p><button class="btn btn-large btn-primary" type="button">Pelajari lebih lanjut</button></p>
			</div>
		</div>
	</div>
	<!-- /call to action start project-->

	
	
	

<?php
	include 'module/footerv4.php';
?>