<?php
	include 'module/headerv4.php';
	//include 'module/infographic-global.php';
	
?>
	
	<div class="big-map-wrapper">

		<!-- Big map twitgreen just2-->
		<div class="big-maps-wrapper">
			<div class="Flexible-container">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14235824.764561404!2d115.9933789751217!3d-3.119513931174574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2c4c07d7496404b7%3A0xe37b4de71badf485!2sIndonesia!5e1!3m2!1sen!2s!4v1400227945625" width="600" height="450" frameborder="0" style="border:0"></iframe>
			</div>
		</div>
		<!-- /Big map twitgreen-->

		<!-- featured projects twitgreen-->
		<div class="big-project-wrapper">
			<div class="container">

				<!-- list project slider twitgreen-->
				<div class="new-featured-prjc">
					<ul class="bxslider">
					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopertamina.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>
					  
					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopatra.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logojkt.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopertamina.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project bg-white center">
								<img src="upload/logopatra.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>

					  <li>
					  	<div class="span3 twit-feat-project-box">

							<div class="head-feat-project center">
								<img src="upload/logojkt.jpg" class="img-home-mini">
								<h3 class="mini-title">Pertamina 100 Juta Pohon</h3>
							</div>

							<div class="body-feat-project">
								<ul class="feat-project-infos">
									<li><i class="fa fa-pagelines"></i> 98.103456.0599 <span class="pull-right">Pohon</span></li>
									<li><i class="fa fa-slack"></i> 30.365 <span class="pull-right">Lot</span></li>
									<li><i class="fa fa-cube"></i> 54.5678.2345 ha <span class="pull-right">Lahan</span></li>
									<li><i class="fa fa-bank"></i> 12345 <span class="pull-right">Desa</span></li>
								</ul>
							</div>

							<div class="foot-feat-project center">
								<button class="btn" type="button">Info detail</button>
							</div>

						</div>
					  </li>
					</ul>
				</div> <!-- /new-featured-prjc-->
				<!-- /list project slider twitgreen-->
			
			</div>
		</div>
		<!-- /featured projects twitgreen-->

	</div>

	<!-- call to action start project-->
	<div class="twit-how-prj-box">
		<div class="container">
			<div class="center">
				<h2>Bagaimana memulai Project?</h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod <br> tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
				<br/>
				<p><button class="btn btn-large btn-primary" type="button">Pelajari lebih lanjut</button></p>
			</div>
		</div>
	</div>
	<!-- /call to action start project-->
	
	<div class="container">
		<div class="center">
			<br/><br/>
			<img src="img/new-icon-tree.png" alt="">
			<h2>"Memberi Oksigen Untuk Dunia"</h2>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod <br>Lorem ipsum dolor sit amet, <a href="#">consectetuer</a> adipiscing elit, sed.</p>
			<p><button class="btn btn-large btn-primary" type="button">Customer Service</button></p>
		</div>
	</div>

	<div class="container">
	 <div class="row">	
		<div class="span9">
			<div class="prlx-1"></div>
			<h5>PROJECT :</h5>
			<ul id="new-projects" class="thumbnails">
				<?php
					$i = 0;
					while ( $i <= 5) {
						include 'module/content-project.php';
						$i ++;
					}
				?>
			</ul>
			<button class="btn btn-success right" type="button"><i class="fa fa-camera-retro fa-lg"></i> Lihat semua project</button>
		</div>

		<div class="span3">
			<h4 class="text-white">Stakeholder</h4>
			<div class="panel">
				<ul class="footer-widget-list">
					<li><a href="#">User</a></li>
					<li><a href="#">Donatur</a></li>
					<li><a href="#">Desa</a></li>
					<li><a href="#">Lot</a></li>
				</ul>
			</div>
			<p>&nbsp;</p>
			<h5>TAHAPAN :</h5>
			<div class="panel">
				<ul class="footer-widget-list">
					<li><a href="#">Draft</a></li>
					<li><a href="#">Offering</a></li>
					<li><a href="#">Plant</a></li>
					<li><a href="#">Ready to plant</a></li>
					<li><a href="#">Planting</a></li>
					<li><a href="#">Planted</a></li>
					<li><a href="#">Ready to plant</a></li>
					<li><a href="#">Verified</a></li>
					<li><a href="#">Saving Trees</a></li>
					<li><a href="#">Growing</a></li>
					<li><a href="#">Production</a></li>
					<li><a href="#">Sustained</a></li>
					<li><a href="#">Growth</a></li>
				</ul>
			</div>
		
		</div>
      </div>
	</div>

	<!-- call to action join relawan-->
	<div class="twit-how-join-box">
		<div class="container">
			<div class="center">
				<h2>Bergabung menjadi relawan?</h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod <br> tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
				<br/>
				<p><button class="btn btn-large btn-primary" type="button">Bergabung</button></p>
			</div>
		</div>
	</div>
	<!-- /call to action join relawan-->

	

<?php
	include 'module/footerv4-b.php';
?>